/**
*@author: Daniel Reinosa Luque
*@version: "1.0-SNAPSHOT"
*/
import java.io.File
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.forEachLine
import kotlin.io.path.readLines
import kotlin.io.path.readText

/**
Implementacion de varables para los colores, el scanner y la variable error que retornará error del sistema
en caso de no funcionamiento de la aplicacion.
 */
val file = File("src/main/resources/ranking.txt")
const val colorGris = "\u001b[37m"
const val colorGroc = "\u001b[33m"
const val colorVerd = "\u001b[32m"
const val colorRed = "\u001b[31m"
const val colorReset = "\u001b[0m"
val scanner = Scanner(System.`in`)
var error = false


fun start (language:Int,counter: Int,nickName: String){
    val solution = randomWord(language)
    val userWord = scanner.next().uppercase()
    when(userWord){
        "PLAY" ->{
            play(solution,counter,nickName)

        }
        "HELP" ->{
            help()
        }
        "RANK" ->{
            ranking()
        }
    }
}
fun main() {
    do {
        var counter = 5
        println("Indica el teu nickname")
        val nickName = scanner.next()
        println("Select your language")
        println("1-ESP | 2-CAT | 3-ENG")
        val language = scanner.nextInt()
        bienvenida(language)
        start(language,counter,nickName)

    }while (error)
}


fun save(nickName:String,counter:Int){

    var idUsuario = 1
    file.forEachLine {
        idUsuario++
    }
    val salida = "$idUsuario;$nickName;${counter.toInt()}\n"
    file.appendText(salida)
}
fun bienvenida(language:Int){
    when(language){
        1 ->{
            println("${colorGroc}. . . . . . . . . . . . . . . . . . . . . . Bienvenido a WORDLE . . . . . . . . . . . . . . . .$colorReset")
            println("Indica${colorVerd} PLAY$colorReset para jugar,${colorGroc}RANK$colorReset si deseas ver ranking o ${colorRed}HELP$colorReset si todavia no sabes las normas.")
            println("¡¡Buena suertee!!")
        }
        2 ->{
            println("${colorGroc}. . . . . . . . . . . . . . . . . . . . . . Benvingut a WORDLE . . . . . . . . . . . . . . . .$colorReset")
            println("Indica${colorVerd} PLAY$colorReset per a jugar,${colorGroc}RANK$colorReset si desitjes veure el ranking o ${colorRed}HELP$colorReset si encara no saps les normes.")
            println("¡¡Bona sooort!!")
        }
        3 ->{
            println("${colorGroc}. . . . . . . . . . . . . . . . . . . . . . Welcome to WORDLE . . . . . . . . . . . . . . . .$colorReset")
            println("Select${colorVerd} PLAY$colorReset for play,${colorGroc}RANK$colorReset if you want see ranking or ${colorRed}HELP$colorReset for look how to play.")
            println("¡¡Goood luck!!")
        }
    }
}

/**
*LLama al fichero "wordListESP.txt" donde se encuentran las posibles palabras de respuesta, despues coge una palabra random
*y la retorna en forma de "randomWord".
*/
fun randomWord(fileNamenguage: Int): String {
    var solution = ""
    when(fileNamenguage){
        1 ->{
            val callFile = File("src/main/resources/wordListESP.txt")
            if (!callFile.exists()) {
                return "Error: Archivo no encontrado."
            }
            else{
                val readFile = callFile.readLines()
                if (readFile.isEmpty()) {
                    return "Error: Archivo vacío."
                }
                val randomWord = readFile.random()
                solution = randomWord
            }}
        2 ->{
            val callFile = File("src/main/resources/wordListCAT")
            if (!callFile.exists()) {
                return "Error: Arxiu no trobat."
            }
            else{
                val readFile = callFile.readLines()
                if (readFile.isEmpty()) {
                    return "Error: Arxiu buit."
                }
                val randomWord = readFile.random()
                solution = randomWord
            }}
        3 ->{
            val callFile = File("src/main/resources/wordListING")
            if (!callFile.exists()) {
                return "Error: file not found."
            }
            else{
                val readFile = callFile.readLines()
                if (readFile.isEmpty()) {
                    return "Error: file empty."
                }
                val randomWord = readFile.random()
                 solution = randomWord
            }}

    }
    return  solution
}

fun play(solution:String,counter: Int,nickName: String){
    var counter = counter
    println(solution)
    for (j in 0..6){
        println("Intenta adivinar la palabra de cinco letras!")
        val selectedWord = scanner.next()
        if (counter != 0){
            counter --
            for (i in colorRules(selectedWord,solution)){
                print(i)
            }
            println()
            println()
            println("   -Vidas :$counter")
        }
        else if (counter == 0){
            println("$colorRed Has perdido, prueba de nuevo$colorReset")
            println("La palabra correcta es:$solution")
            error = true
        }
    }
    save(nickName,counter)
}
/**
 *La siguiente funcion "colorRules", llama a la palabra de cinco letras con el tipo de dato String y la palabra generada
 *aleatoriamente.
 *Devolverá la comparacion de estas dos palabras retornando un String modificado por los colores.
 *
 *@param: selectedWord
 */
fun colorRules(selectedWord: String,solution:String): MutableList<String>{
    var coloredWord = mutableListOf<String>()
    // win

    for (i in 0..selectedWord.lastIndex) {

        // error
        if (selectedWord.length !=5) {
            println("La palabra no contiene 5 letras repite de nuevo.")
            error(true)
        }
        //color verd
        if (selectedWord[i] == solution[i]) {
            coloredWord.add("$colorVerd${selectedWord[i]}$colorReset")
        }
        // correcte pero a un altre lloc
        else if ((selectedWord[i] != solution[i]) && (selectedWord[i] in solution)){
            when(differentW(selectedWord,solution)){
                1 ->{
                    coloredWord.add("$colorRed${selectedWord[i]}$colorReset")
                }
                2 ->{
                    coloredWord.add("$colorGroc${selectedWord[i]}$colorReset")
                }
            }

        }



            // print incorrecte
            else{
                coloredWord.add("$colorRed${selectedWord[i]}$colorReset")

    }
}
    if (selectedWord == solution) {
        coloredWord.add("$colorVerd${selectedWord}$colorReset")
        win()
    }
    return coloredWord
}
/**
 * Compara dos palabras y determina cuál de ellas tiene más letras repetidas en la otra.
 * Retorna 1 si la palabra1 tiene más letras repetidas en la palabra2.
 * Retorna 2 si la palabra2 tiene más letras repetidas en la palabra1.
 *
 * @param palabra1 La primera palabra a comparar.
 * @param palabra2 La segunda palabra a comparar.
 * @return Un entero indicando qué palabra tiene más letras repetidas en la otra.
 */
fun differentW(palabra1: String, palabra2: String): Int {
    val letrasRepetidas1 = mutableListOf<Char>()
    val letrasRepetidas2 = mutableListOf<Char>()

    // Obtener las letras repetidas de la palabra 1
    for (letra in palabra1) {

        if (letra in palabra2 && letra !in letrasRepetidas1) {
            letrasRepetidas1.add(letra)
        }
    }

    // Obtener las letras repetidas de la palabra 2
    for (letra in palabra2) {
        if (letra in palabra1 && letra !in letrasRepetidas2) {
            letrasRepetidas2.add(letra)
        }
    }

    return if (letrasRepetidas1.size > letrasRepetidas2.size) 1
    else 2
}
/**
*Printa el siguiente texto para indicar que has ganado.
*/
fun win(){
    println("${colorVerd}¡HAS GANADO CRACK!$colorReset")
    kotlin.system.exitProcess(0)
}

/**
 *Llamada por el comado HELP se printara el siguiente texto.
 *Al finalizar se preguntara al usuario si quiere empezar la partida.
 */
fun help(){
    println(". . . . . . . . . . . . . . . . . . . . . . . . . . . W O R D L E . . . . . . . . . . . . . . . . . . . . . . . . . . .")
    println("El objetivo del juego es adivinar una palabra concreta de cinco letras en un máximo de seis intentos.\"")
    println("Escriba en primera línea una palabra de cinco letras de su elección")
    println("El jugador escribe en la primera línea una palabra de cinco letras de su elección e introduce su propuesta.\n" +
            "                 \n" +
            "                 Después de cada proposición, las letras aparecen en color:\n" +
            "                 $colorGris El fondo gris representa las letras que no están en la palabra buscada.$colorReset\n" +
            "                 $colorGroc El fondo amarillo representa las letras que se encuentran en otros lugares de la palabra. $colorReset \n" +
            "                 $colorVerd El fondo verde representa las letras que están en el lugar correcto en la palabra a encontrar.$colorReset \n")
    println("¿Quieres empezar la partida?$colorVerd S/N $colorReset")
    var answer = scanner.next().uppercase()
    if(answer == "S")return bienvenida(3)
    else if(answer == "N")answer == "HELP"
}
/**
 *LLama al fichero ranking para cargarlo.
 *@param filePath llama a dicho fichero.
 */
fun ranking(filePath:String ="src/main/resources/ranking.txt"){
    val fileContent = File(filePath).readLines()
    for (i in fileContent){
        val line = i.split(";")
        println("ID:${line[0]}|NICK:${line[1]}|PUNTUATION:${line[2]}")
    }

}
