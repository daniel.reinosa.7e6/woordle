# Wordle

## Descripción
Wordle es un juego de adivinanzas en el que los jugadores intentan encontrar una palabra de cinco letras en un máximo de seis intentos. El juego ofrece opciones de idioma y tiene un sistema de clasificación para registrar las puntuaciones de los jugadores.

## Características
- **Selección de idioma:** El juego permite al jugador elegir entre español, catalán e inglés.
- **Juego principal:** El jugador intenta adivinar la palabra oculta proporcionando palabras de cinco letras.
- **Colores de retroalimentación:** Después de cada intento, se muestran colores para indicar las coincidencias y discrepancias entre la palabra propuesta y la palabra oculta.
- **Sistema de puntuación:** El juego asigna una puntuación al jugador según su rendimiento.
- **Sistema de clasificación:** El juego guarda las puntuaciones más altas de los jugadores en un archivo de clasificación.

## Instrucciones de ejecución
1. Clona este repositorio en tu máquina local.
2. Abre el proyecto en tu IDE favorito.
3. Compila y ejecuta el archivo `Main.kt`.
4. Sigue las instrucciones en la consola para jugar a Wordle.

## Archivos de recursos
- **wordListESP.txt:** Archivo que contiene una lista de palabras en español.
- **wordListCAT.txt:** Archivo que contiene una lista de palabras en catalán.
- **wordListENG.txt:** Archivo que contiene una lista de palabras en inglés.
